#include <stdint.h>
#include <stdbool.h>
#include "linux_types.h"

/* #define GPIO0_27_PIN 0x08000000     BAT-LEV-1  */
/* #define GPIO0_11_PIN 0x00000800     BAT-LEV-2  */
/* #define GPIO1_29_PIN 0x20000000     BAT-LEV-3  */
/* #define GPIO0_26_PIN 0x04000000     BAT-LEV-4  */

// #define GPIO0_START_ADDR 0x44E07000
// #define GPIO1_START_ADDR 0x4804C000
// #define GPIO2_START_ADDR 0x481AC000
// #define GPIO3_START_ADDR 0x481ae000
// #define GPIO_DATAIN (0x138)
// #define GPIO_DATAOUT (0x13C)
// #define GPIO_CLEARDATAOUT (0x190)
// #define GPIO_SETDATAOUT (0x194)

#define DPRAM_SHARED 0x00010000

/* Note: PRU number should be defined prior to pru specific headers */
#define PRU1
#include "pru_defs.h"
#include "pru_pwm.h"
#include "pru_hal.h"
#include "uart.h"

#define MAX_CHS 8

char                    data_str[16];

volatile uint32_t       pwm_period[MAX_CHS];
volatile uint32_t       pwm_pulse_width[MAX_CHS];
volatile uint32_t       pwm_pulse_width_sorted[MAX_CHS];
volatile uint8_t        pwm_enabled[MAX_CHS];

/* RCOut[0] -> PWM-1 (R30-d8)
 * RCOut[1] -> PWM-2 (R30-d10)
 * RCOut[2] -> PWM-3 (R30-d9)
 * RCOut[3] -> PWM-4 (R30-d11)
 * RCOut[4] -> PWM-5 (R30-d6)
 * RCOut[5] -> PWM-6 (R30-d7)
 * RCOut[7] -> PWM-8 (R30-d5) PWM-7 is unusable since it's a boot config pin
 * RCOut[8]  (unused)
 */
//const uint32_t       	pwm_output_map_mask[] = {0x00000100, 0x00000400, 0x00000200, 0x00000800,
//                                                 0x00000040, 0x00000080, 0x00000020, 0x00000000};

const uint32_t       	pwm_output_map_mask[] = {0x00000400/* PWM2 */, 0x00000040/* PWM5 */,
                                                 0x00000080/* PWM6 */, 0x00000200/* PWM3 */,
                                                 0x00000100/* PWM1 */, 0x00000800/* PWM4 */,
                                                 0x00000020/* PWM8 */, 0x00000000/* N/A */};

//      pwm_period[i] = 0x003d0900;     /* 50 Hz */
//      pwm_period[i] = 0x0032dcd5;     /* 60 Hz */
//      pwm_period[i] = 0x00186a00;     /* 125 Hz */
//      pwm_period[i] = 0x000c3500;     /* 250 Hz */

static void pwm_setup(void)
{
	uint8_t i;

//	i = (uint8_t)val;

	for (i=0; i<MAX_CHS; i++)
	{
		pwm_period[i] = 0xc3500;     /* 250 Hz */
	}

        /* This is the unsorted version of the PWM data
         * Times are given in IEP counts (200 cnts/us)
         * eg. 1ms = 1000us = 200,000 IEP cnts
         */
#if 0
	pwm_pulse_width[0] = 200000;       /* 1000 us */
	pwm_pulse_width[1] = 220000;       /* 1100 us */
	pwm_pulse_width[2] = 240000;       /* 1200 us */
	pwm_pulse_width[3] = 260000;       /* 1300 us */
	pwm_pulse_width[4] = 280000;       /* 1400 us */
	pwm_pulse_width[5] = 300000;       /* 1500 us */
	pwm_pulse_width[6] = 320000;       /* 1600 us */
	pwm_pulse_width[7] = 340000;       /* 1700 us */
#endif
	pwm_pulse_width[0] = 200000;       /* 1000 us */
	pwm_pulse_width[1] = 200000;       /* 1000 us */
	pwm_pulse_width[2] = 200000;       /* 1000 us */
	pwm_pulse_width[3] = 200000;       /* 1000 us */
	pwm_pulse_width[4] = 200000;       /* 1000 us */
	pwm_pulse_width[5] = 200000;       /* 1000 us */
	pwm_pulse_width[6] = 200000;       /* 1000 us */
	pwm_pulse_width[7] = 200000;       /* 1000 us */

	/* Enable/Disable all channels */
	for (i=0; i<MAX_CHS; i++)
	{
		pwm_enabled[i] = 1;
	}

	/* Setup DPSHRAM - each PWM period is defined, however all must be the same for now */
	for (i=0; i<MAX_CHS; i++)
	{
		PWM_INFO->channel[i].time_t = 0x3d0900;
	//	PWM_CMD->periodhi[i][1] = 0x3d0900;
	}

	/* Again, the unsorted version of the PWM data */
#if 0
	PWM_INFO->channel[0].time_high = 200000;
	PWM_INFO->channel[1].time_high = 220000;
	PWM_INFO->channel[2].time_high = 240000;
	PWM_INFO->channel[3].time_high = 260000;
	PWM_INFO->channel[4].time_high = 280000;
	PWM_INFO->channel[5].time_high = 300000;
	PWM_INFO->channel[6].time_high = 320000;
	PWM_INFO->channel[7].time_high = 340000;
#endif

#if 1
	PWM_INFO->channel[0].time_high = 200000;
	PWM_INFO->channel[1].time_high = 200000;
	PWM_INFO->channel[2].time_high = 200000;
	PWM_INFO->channel[3].time_high = 200000;
	PWM_INFO->channel[4].time_high = 200000;
	PWM_INFO->channel[5].time_high = 200000;
	PWM_INFO->channel[6].time_high = 200000;
	PWM_INFO->channel[7].time_high = 200000;

	/* OFF mask TBD, all 8 PWM on for now */
	PWM_INFO->channelenable = 0x000000ff;
#endif

#if 0
	PWM_CMD->periodhi[0][0] = 200001;	/* time ON */
	PWM_CMD->periodhi[1][0] = 200002;
	PWM_CMD->periodhi[2][0] = 200003;
	PWM_CMD->periodhi[3][0] = 200004;
	PWM_CMD->periodhi[4][0] = 200005;
	PWM_CMD->periodhi[5][0] = 200006;
	PWM_CMD->periodhi[6][0] = 200007;
	PWM_CMD->periodhi[7][0] = 200008;

	PWM_CMD->enmask = 0x000000ff;
#if 0
	pwm_output_map_mask[0] = 0x00000100;
	pwm_output_map_mask[1] = 0x00000400;
	pwm_output_map_mask[2] = 0x00000200;
	pwm_output_map_mask[3] = 0x00000800;
	pwm_output_map_mask[4] = 0x00000040;
	pwm_output_map_mask[5] = 0x00000080;
	pwm_output_map_mask[6] = 0x00000010;
	pwm_output_map_mask[7] = 0x00000020;
#endif

#endif
}

static void bubble_sort_with_index (uint8_t sorted_indexes[])
{
	uint8_t     i, j;
	uint32_t    temp_val;
	uint8_t     temp_index;

	/* Populate the sorted array with the unsorted data */
	for (i=0; i<MAX_CHS; i++)
	{
		pwm_pulse_width_sorted[i] = pwm_pulse_width[i];
		sorted_indexes[i] = i;
/*		Offset for SVO1-8 PRU bit offset of 4  */
/*		sorted_indexes[i] = i + 4; */
	}

	for (i=0; i<MAX_CHS; i++)
	{
		for (j=i; j<MAX_CHS; j++)
		{
			/* Sort all values from smallest to largest */
			if (pwm_pulse_width_sorted[j] < pwm_pulse_width_sorted[i])
			{
				temp_val = pwm_pulse_width_sorted[i];
				temp_index = sorted_indexes[i];
				pwm_pulse_width_sorted[i] = pwm_pulse_width_sorted[j];
				pwm_pulse_width_sorted[j] = temp_val;
				sorted_indexes[i] = sorted_indexes[j];
				sorted_indexes[j] = temp_index;
			}
		}
	}
}

static inline u32 read_PIEP_COUNT(void)
{
	return PIEP_COUNT;
}

int main(int argc, char *argv[])
{
	uint8_t			i;
	volatile uint32_t	cnt_now;
	volatile uint32_t	cnt_next;
	volatile uint32_t	lLoopCount = 0;
	volatile uint16_t	si_index = 0;
	volatile uint32_t	lLedCount = 0;
	volatile bool		bLedState = false;
	volatile bool		bDone = false;

	uint8_t			sorted_indexes[MAX_CHS];

	uint32_t		reg_bit_mask;
	uint32_t		next_on_cnt[MAX_CHS];
	uint32_t		next_off_cnt[MAX_CHS];
	uint32_t		cnt_ready;
	uint32_t		staging_cnt;

	uint32_t		last_read_enmask;       /* enable mask */
        uint32_t		latest_enmask;

	/* enable OCP master port */
	PRUCFG_SYSCFG &= ~SYSCFG_STANDBY_INIT;
	PRUCFG_SYSCFG = (PRUCFG_SYSCFG &
					~(SYSCFG_IDLE_MODE_M | SYSCFG_STANDBY_MODE_M)) |
					SYSCFG_IDLE_MODE_NO | SYSCFG_STANDBY_MODE_NO;

	/* our PRU wins arbitration */
	PRUCFG_SPP |=  SPP_PRU1_PAD_HP_EN;

	/* configure timer */
	PIEP_GLOBAL_CFG = GLOBAL_CFG_DEFAULT_INC(1) | GLOBAL_CFG_CMP_INC(1);
	PIEP_CMP_STATUS = CMD_STATUS_CMP_HIT(1); /* clear the interrupt */
	PIEP_CMP_CMP1   = 0x0;
	PIEP_CMP_CFG |= CMP_CFG_CMP_EN(1);
	PIEP_GLOBAL_CFG |= GLOBAL_CFG_CNT_ENABLE;

	/* Setup some default values */
	pwm_setup();

	/* Sort pulse width data in asending order */
	bubble_sort_with_index (sorted_indexes);

	/* Initialize count */
	cnt_now = read_PIEP_COUNT();

	/* Initialize UART-1 */
	uart_initialize();
	uart_xmit_str ("Hello PRU!\n");

	/* Setup turn ON counts */
	next_on_cnt[0] = cnt_now + 200;
	for (i=1; i<MAX_CHS; i++)
	{
		next_on_cnt[i] = next_on_cnt[i-1] + 200;
	}

	/* Setup turn OFF counts */
	for (i=0; i<MAX_CHS; i++)
	{
		next_off_cnt[i] = next_on_cnt[i] + pwm_pulse_width_sorted[i];
	}


//	cnt_next = cnt_now + 800000;
	last_read_enmask = PWM_INFO->channelenable;
//	last_read_enmask = PWM_CMD->enmask;

//	*((volatile unsigned long *)0x44E07194) = 0x08000000;  /* BAT-LEV-1 */
//	*((volatile unsigned long *)0x44E07194) = 0x00000800;  /* BAT-LEV-2 */

	while (!bDone)
	{
		/* Wait for turn ON counts */
		for (i=0; i<MAX_CHS; i++)
		{
			if (pwm_enabled[sorted_indexes[i]])
			{
				cnt_ready = next_on_cnt[i];
				/* Offset of 4 for SVO1-8 PRU bit offset */
				reg_bit_mask = pwm_output_map_mask[sorted_indexes[i]];
				while ((read_PIEP_COUNT() - cnt_ready) & 0x80000000);
				__R30 |= reg_bit_mask;
			}
		}

		/* Wait for turn OFF counts */
		for (i=0; i<MAX_CHS; i++)
		{
			if (pwm_enabled[sorted_indexes[i]])
			{
				cnt_ready = next_off_cnt[i];
				/* Offset of 4 for SVO1-8 PRU bit offset */
				reg_bit_mask = ~(pwm_output_map_mask[sorted_indexes[i]]);
				while ((read_PIEP_COUNT() - cnt_ready) & 0x80000000);
				__R30 &= reg_bit_mask;
			}
		}

                /* Get potentially new pulse width data and sort it */
#define ABC_1 2
#ifdef ABC_1
		for (i=0; i<MAX_CHS; i++)
		{
			pwm_pulse_width[i] = PWM_INFO->channel[i].time_high;
//			pwm_pulse_width[i] = PWM_CMD->periodhi[i][0];
		}

                bubble_sort_with_index (sorted_indexes);
#endif

		/* Advance channel timers to period - staggered ON times by 1 us */
		next_on_cnt[0] += pwm_period[0];
		for (i=1; i<MAX_CHS; i++)
		{
			next_on_cnt[i] = next_on_cnt[i-1] + 200;
		}
		/* Next turn OFF */
		for (i=0; i<MAX_CHS; i++)
		{
			next_off_cnt[i] = next_on_cnt[i] + pwm_pulse_width_sorted[i];
		}

#if 0
		for (i=0; i<MAX_CHS; i++)
			PWM_CMD->hilo_read[i][1] = pwm_pulse_width[i];
#endif

		latest_enmask = PWM_INFO->channelenable;
//		latest_enmask = PWM_CMD->enmask;
//		latest_enmask = 0xff;

		if (last_read_enmask != latest_enmask)
		{
			/* Enable/Disable */
			for (i=0; i<MAX_CHS; i++)
			{
				if (latest_enmask & (1U << i))
					pwm_enabled[i] = 1;
				else
				{
					/* Turn off any disabled channel */
					reg_bit_mask = ~(pwm_output_map_mask[i]);
					__R30 &= reg_bit_mask;
//					__R30 &= ~(1U << i);
					pwm_enabled[i] = 0;
				}
			}
			last_read_enmask = latest_enmask;
		}

		/* Spend some time doing busy work till just before next PW cycle */
		staging_cnt = next_on_cnt[0] - 1000;
		while ((read_PIEP_COUNT() - staging_cnt) & 0x80000000)
		{
			for (i=0; i<10; i++);
		};

		// Exit if we receive a Host->PRU1 interrupt
		if (__R31 & 0x80000000)
		{
			uart_xmit_str ("Goodbye PRU!\n");
			bDone = true;
		}

#if 0
		if (100 < ++lLoopCount)
		{
			latest_enmask = PWM_INFO->channelenable;
//			latest_enmask = PWM_CMD->enmask;
			data_str[0] = '0';
			data_str[1] = 'x';
			decode_u32 (latest_enmask, &data_str[2]);
                        data_str[10] = '\n';
                        data_str[11] = '\0';
  //			uart_xmit_str ("EM: ");
  //                      uart_xmit_str (data_str);
			lLoopCount = 0;

			switch (si_index)
			{
				case 0:
  					uart_xmit_str ("PWM[0]: 0x");
//  					uart_xmit_str ("PWM[8]: 0x");
//					decode_u32 (PWM_CMD->periodhi[0][0], &data_str[0]);
					decode_u32 (PWM_INFO->channel[0].time_high, &data_str[0]);
					data_str[8] = '\n';
					data_str[9] = '\0';
					uart_xmit_str (data_str);
					si_index = 1;
					break;
				case 1:
  					uart_xmit_str ("PWM[1]: 0x");
//					decode_u32 (PWM_CMD->periodhi[1][0], &data_str[0]);
					decode_u32 (PWM_INFO->channel[1].time_high, &data_str[0]);
					data_str[8] = '\n';
					data_str[9] = '\0';
					uart_xmit_str (data_str);
					si_index = 2;
					break;
				case 2:
  					uart_xmit_str ("PWM[2]: 0x");
//					decode_u32 (PWM_CMD->periodhi[2][0], &data_str[0]);
					decode_u32 (PWM_INFO->channel[2].time_high, &data_str[0]);
					data_str[8] = '\n';
					data_str[9] = '\0';
					uart_xmit_str (data_str);
					si_index = 3;
					break;
				case 3:
  					uart_xmit_str ("PWM[3]: 0x");
//					decode_u32 (PWM_CMD->periodhi[3][0], &data_str[0]);
					decode_u32 (PWM_INFO->channel[3].time_high, &data_str[0]);
					data_str[8] = '\n';
					data_str[9] = '\0';
					uart_xmit_str (data_str);
					si_index = 4;
					break;
				case 4:
  					uart_xmit_str ("PWM[4]: 0x");
//					decode_u32 (PWM_CMD->periodhi[4][0], &data_str[0]);
					decode_u32 (PWM_INFO->channel[4].time_high, &data_str[0]);
					data_str[8] = '\n';
					data_str[9] = '\0';
					uart_xmit_str (data_str);
					si_index = 5;
					break;
				case 5:
  					uart_xmit_str ("PWM[5]: 0x");
//					decode_u32 (PWM_CMD->periodhi[5][0], &data_str[0]);
					decode_u32 (PWM_INFO->channel[5].time_high, &data_str[0]);
					data_str[8] = '\n';
					data_str[9] = '\0';
					uart_xmit_str (data_str);
					si_index = 6;
					break;
				case 6:
  					uart_xmit_str ("PWM[6]: 0x");
//					decode_u32 (PWM_CMD->periodhi[6][0], &data_str[0]);
					decode_u32 (PWM_INFO->channel[6].time_high, &data_str[0]);
					data_str[8] = '\n';
					data_str[9] = '\0';
					uart_xmit_str (data_str);
					si_index = 7;
					break;
				case 7:
  //					uart_xmit_str ("PWM[7]: 0x");
  					uart_xmit_str ("PWM[7]: 0x");
//					decode_u32 (PWM_CMD->periodhi[7][0], &data_str[0]);
					decode_u32 (PWM_INFO->channel[7].time_high, &data_str[0]);
					data_str[8] = '\n';
					data_str[9] = '\0';
					uart_xmit_str (data_str);
					si_index = 0;
					break;
				default:
					si_index = 0;
			}
#if 0
			switch (si_index)
			{
				case 0:
  					uart_xmit_str ("SI[0]: 0x");
					decode_u8 (sorted_indexes[0], &data_str[0]);
					data_str[2] = '\n';
					data_str[3] = '\0';
					uart_xmit_str (data_str);
					si_index = 1;
					break;
				case 1:
  					uart_xmit_str ("SI[1]: 0x");
					decode_u8 (sorted_indexes[1], &data_str[0]);
					data_str[2] = '\n';
					data_str[3] = '\0';
					uart_xmit_str (data_str);
					si_index = 2;
					break;
				case 2:
  					uart_xmit_str ("SI[2]: 0x");
					decode_u8 (sorted_indexes[2], &data_str[0]);
					data_str[2] = '\n';
					data_str[3] = '\0';
					uart_xmit_str (data_str);
					si_index = 3;
					break;
				case 3:
  					uart_xmit_str ("SI[3]: 0x");
					decode_u8 (sorted_indexes[3], &data_str[0]);
					data_str[2] = '\n';
					data_str[3] = '\0';
					uart_xmit_str (data_str);
					si_index = 4;
					break;
				case 4:
  					uart_xmit_str ("SI[4]: 0x");
					decode_u8 (sorted_indexes[4], &data_str[0]);
					data_str[2] = '\n';
					data_str[3] = '\0';
					uart_xmit_str (data_str);
					si_index = 5;
					break;
				case 5:
  					uart_xmit_str ("SI[5]: 0x");
					decode_u8 (sorted_indexes[5], &data_str[0]);
					data_str[2] = '\n';
					data_str[3] = '\0';
					uart_xmit_str (data_str);
					si_index = 6;
					break;
				case 6:
  					uart_xmit_str ("SI[6]: 0x");
					decode_u8 (sorted_indexes[6], &data_str[0]);
					data_str[2] = '\n';
					data_str[3] = '\0';
					uart_xmit_str (data_str);
					si_index = 0;
					break;
				default:
					si_index = 0;
			}
#endif
		}
#endif
		if (50 < ++lLedCount)
		{
			if (bLedState)
			{
				/* Clear LED */
				*((volatile unsigned long *)0x44E07190) = 0x00000800;  /* BAT-LEV-2 */
				bLedState = false;
			}
			else
			{
				/* Set LED */
				*((volatile unsigned long *)0x44E07194) = 0x00000800;  /* BAT-LEV-2 */
				bLedState = true;
			}
			lLedCount = 0;
		}
	}

	__R31 = 35;	// PRUEVENT_0 on PRU_R31_VEC_VALID
	__halt();

	return 0;
}

