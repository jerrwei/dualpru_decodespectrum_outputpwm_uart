#include <stdint.h>
#include <stdbool.h>
//#include "linux_types.h"

//#include "pru_defs.h"
//#include "pru_pwm.h"
#include "pru_uart.h"
//#include "pru_hal.h"

#include "uart.h"

typedef union u32_u16_u8_data
{
	uint8_t	 u8_data[4];
	uint16_t u16_data[2];
	uint32_t u32_data;
} u32_u16_u8_data;

/* #include "resource_table_empty.h" */

/* The FIFO size on the PRU UART is 16 bytes; however, we are (arbitrarily)
 * only going to send 8 at a time */
#define FIFO_SIZE 16
#define MAX_CHARS 8
#define NULL ((char) 0)

/* This hostBuffer structure is temporary but stores a data buffer */
struct {
	uint8_t msg; // Not used today
	uint8_t data[FIFO_SIZE];
} hostBuffer;


void uart_initialize (void)
{
	/* Set up UART to function at 115200 baud - DLL divisor is 104 at 16x oversample, 192MHz / 104 / 16 = ~115200 */
	CT_UART.DLL = 104;
	CT_UART.DLH = 0;
	CT_UART.MDR = 0x0;

	/* Enable Interrupts in UART module. This allows the main thread to poll for
	 * Receive Data Available and Transmit Holding Register Empty */
	CT_UART.IER = 0x7;

	/* If FIFOs are to be used, select desired trigger level and enable
	 * FIFOs by writing to FCR. FIFOEN bit in FCR must be set first before
	 * other bits are configured */
	CT_UART.FCR = (0x80) | (0x8) | (0x4) | (0x2) | (0x01); // 8-byte RX FIFO trigger

	/* Choose desired protocol settings by writing to LCR */
	/* 8-bit word, 1 stop bit, no parity, no break control and no divisor latch */
	CT_UART.LCR = 3;

	/* Enable loopback for test */
	/*
	 * NOTE!
	 * loopback will prevent the UART from sending data to the output
	 * pins. Remember to disable loopback in the MCR register before
	 * looking for signals on your UART pins.
	 */
	CT_UART.MCR = 0x00;

	/* Choose desired response to emulation suspend events by configuring
	 * FREE bit and enable UART by setting UTRST and URRST in PWREMU_MGMT */
	/* Allow UART to run free, enable UART TX/RX */
	CT_UART.PWREMU_MGMT_bit.FREE = 1;
	CT_UART.PWREMU_MGMT_bit.URRST = 1;
	CT_UART.PWREMU_MGMT_bit.UTRST = 1;

	/* Turn off RTS and CTS functionality */
	CT_UART.MCR_bit.AFE = 0x0;
	CT_UART.MCR_bit.RTS = 0x0;
}

void uart_xmit_str (volatile char *str)
{
	uint8_t cnt, index = 0;

	while (1)
	{
		cnt = 0;

		/* Wait until the TX FIFO and the TX SR are completely empty */
		while (!CT_UART.LSR_bit.TEMT);

		while (str[index] != NULL && cnt < MAX_CHARS)
		{
			CT_UART.THR_bit.DATA = str[index];
			index++;
			cnt++;
		}
		if (str[index] == NULL)
			break;
	}

	/* Wait until the TX FIFO and the TX SR are completely empty */
	while (!CT_UART.LSR_bit.TEMT);
}

void decode_u8 (uint8_t u8_val, char str_data[])
{
	uint8_t val_low = u8_val & 0xf;
	uint8_t val_high = u8_val >> 4;

	if (val_low <= 9)
		str_data[1] = (char)(val_low + 0x30);
	else
		str_data[1] = (char)(val_low + 0x37);

	if (val_high <= 9)
		str_data[0] = (char)(val_high + 0x30);
	else
		str_data[0] = (char)(val_high + 0x37);
}

void decode_u16 (uint16_t u16_val, char str_data[])
{
	u32_u16_u8_data un_data;

	un_data.u16_data[0] = u16_val;

	decode_u8 (un_data.u8_data[1], &str_data[0]);
	decode_u8 (un_data.u8_data[0], &str_data[2]);
}

void decode_u32 (uint32_t u32_val, char str_data[])
{
	u32_u16_u8_data un_data;

	un_data.u32_data = u32_val;

	decode_u8 (un_data.u8_data[3], &str_data[0]);
	decode_u8 (un_data.u8_data[2], &str_data[2]);
	decode_u8 (un_data.u8_data[1], &str_data[4]);
	decode_u8 (un_data.u8_data[0], &str_data[6]);
}
