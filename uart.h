#ifndef UART_H_INCLUDED
#define UART_H_INCLUDED

void uart_initialize (void);
void uart_xmit_str (volatile char *str);
void decode_u8 (uint8_t u8_val, char str_data[]);
void decode_u16 (uint16_t u16_val, char str_data[]);
void decode_u32 (uint32_t u32_val, char str_data[]);

#endif /* UART_H_INCLUDED */
